from xpresso.ai.core.data.visualization.report.report import ReportParam
from xpresso.ai.core.data.visualization.report.report import Report
from xpresso.ai.core.data.visualization.report.report_univariate import \
    UnivariateReport
from xpresso.ai.core.data.visualization.report.report_multivariate import \
    MultivariateReport
from xpresso.ai.core.data.visualization.report.report_combined import \
    CombinedReport
from xpresso.ai.core.data.visualization.report.report_scatter import \
    ScatterReport

